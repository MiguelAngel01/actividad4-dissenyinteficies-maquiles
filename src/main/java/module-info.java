module es.MAQUILES.DissenyInterficies_Practica4 {
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.base;

    opens es.MAQUILES.DissenyInterficies_Practica4 to javafx.fxml;
    exports es.MAQUILES.DissenyInterficies_Practica4;
}