package es.MAQUILES.DissenyInterficies_Practica4;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;

public class Ejercicio3Controller implements Initializable {

	ObservableList<String> list = FXCollections.observableArrayList("Paladín","Pícaro","Guerrero","Clerigo");
	
	@FXML
	private ListView lista;
	@FXML
	private Label selected;
	@FXML
	private TextField textInsert;
	@FXML
	private TextField textDelete;
	@FXML
	private Label todos;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		lista.setItems(list);
		lista.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		lista.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				selected.setText(newValue);
				
			}
			
		});
		
	}
	
	@FXML
	private void insert() {
		
		list.add(textInsert.getText().toString());
		lista.setItems(list);
		textInsert.clear();

	}
	
	@FXML
	private void delete() {
		
		list.remove(Integer.parseInt(textDelete.getText().toString()) - 1);
		lista.setItems(list);
		textDelete.clear();

	}
	
	@FXML
	private void mostrarTodos() {
		
		String selected = lista.getSelectionModel().getSelectedItems().toString();
		
		String[] all = selected.split(", ");
		
		String mostrar = "";
		
		for (int i = 0; i < all.length; i++) {
			
			mostrar += all[i] + "\n";
			
		}
		
		todos.setText(mostrar);

	}

}
