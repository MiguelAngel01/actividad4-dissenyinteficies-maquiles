package es.MAQUILES.DissenyInterficies_Practica4;

public class Producto {

	private String nombre;
	private String dimensiones;
	private String peso;
	
	public Producto(String nombre, String dimensiones, String peso) {
		super();
		this.nombre = nombre;
		this.dimensiones = dimensiones;
		this.peso = peso;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDimensiones() {
		return dimensiones;
	}

	public String getPeso() {
		return peso;
	}
	
	
	
}
