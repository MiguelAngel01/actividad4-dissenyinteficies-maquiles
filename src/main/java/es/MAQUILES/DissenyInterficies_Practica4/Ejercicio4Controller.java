package es.MAQUILES.DissenyInterficies_Practica4;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class Ejercicio4Controller implements Initializable {

	ObservableList<Producto> list = FXCollections.observableArrayList(new Producto("Caja", "200x150x300", "980g"), new Producto("Cuadro", "100x15x90", "150g"));
	
	@FXML
	private TableView<Producto> tabla;
	@FXML
	private TableColumn<Producto, String> columNom;
	@FXML
	private TableColumn<Producto, String> columDim;
	@FXML
	private TableColumn<Producto, String> columPes;
	@FXML
	private TextField nom;
	@FXML
	private TextField dim;
	@FXML
	private TextField pes;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		columNom.setCellValueFactory(new PropertyValueFactory<>("nombre"));
		columDim.setCellValueFactory(new PropertyValueFactory<>("dimensiones"));
		columPes.setCellValueFactory(new PropertyValueFactory<>("peso"));
		
		tabla.setItems(list);
		
	}
	
	@FXML
	private void insertar() {
		
		list.add(new Producto(nom.getText().toString(), dim.getText().toString(), pes.getText().toString()));

	}

}
