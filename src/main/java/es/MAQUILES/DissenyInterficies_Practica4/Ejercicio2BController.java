package es.MAQUILES.DissenyInterficies_Practica4;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

public class Ejercicio2BController implements Initializable{

	@FXML
	private Text nom;
	@FXML
	private Text ciutat;
	@FXML
	private Text sistema;
	@FXML
	private Text hores;
	@FXML
	private Text comentari;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		nom.setText(Ejercicio2Controller.nombreCompleto);
		ciutat.setText(Ejercicio2Controller.ciudad);
		sistema.setText(Ejercicio2Controller.sistema);
		hores.setText(Ejercicio2Controller.horas);
		comentari.setText(Ejercicio2Controller.comentario);
		
	}

}
