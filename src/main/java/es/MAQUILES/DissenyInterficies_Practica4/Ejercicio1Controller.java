package es.MAQUILES.DissenyInterficies_Practica4;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;

public class Ejercicio1Controller implements Initializable{

	@FXML
	private ToggleGroup operador;
	@FXML
	private Label operator;
	@FXML
	private TextField numero1;
	@FXML
	private TextField numero2;
	@FXML
	private TextField resultado;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		operador.selectedToggleProperty().addListener(new ChangeListener<Toggle>()
        {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle, Toggle new_toggle)
            {
            	RadioButton selectedOperator = (RadioButton) new_toggle;
        		operator.setText(selectedOperator.getText());
            }
        });
		
	}
	
	@FXML
	private void calcular() {
		
		RadioButton selectedOperator = (RadioButton) operador.getSelectedToggle();
		String operator = selectedOperator.getText();
		
		if (operator.equals("+")) {
			int result = Integer.parseInt(numero1.getText().toString()) + Integer.parseInt(numero2.getText().toString());
			
			resultado.setText(String.valueOf(result));
		} else if (operator.equals("-")) {
			int result = Integer.parseInt(numero1.getText().toString()) - Integer.parseInt(numero2.getText().toString());
			
			resultado.setText(String.valueOf(result));
		} else if (operator.equals("*")) {
			int result = Integer.parseInt(numero1.getText().toString()) * Integer.parseInt(numero2.getText().toString());
			
			resultado.setText(String.valueOf(result));
		} else if (operator.equals("/")) {
			if (!numero2.getText().toString().equals("0")) {
				int result = Integer.parseInt(numero1.getText().toString()) / Integer.parseInt(numero2.getText().toString());
				
				resultado.setText(String.valueOf(result));
			}
		} else if (operator.equals("^")) {
			int numero = Integer.parseInt(numero1.getText().toString());
			int total = 1;
			
			for (int i = 0; i < Integer.parseInt(numero2.getText().toString()); i++) {
				
				total = total * numero;
				
			}
			
			resultado.setText(String.valueOf(total));
		}

	}

}
