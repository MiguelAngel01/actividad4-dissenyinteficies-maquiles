package es.MAQUILES.DissenyInterficies_Practica4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Ejercicio2 extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("Actividad2");
		
        try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Ejercicio2.class.getResource("Ejercicio2.fxml"));
			
			GridPane escena = loader.load();
            
			primaryStage.setScene(new Scene(escena));
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		launch(args);
		
	}

}
