package es.MAQUILES.DissenyInterficies_Practica4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Ejercicio4 extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("Actividad4");
		
        try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Ejercicio2.class.getResource("Ejercicio4.fxml"));
			
			AnchorPane escena = loader.load();
            
			primaryStage.setScene(new Scene(escena));
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		launch(args);
		
	}
}